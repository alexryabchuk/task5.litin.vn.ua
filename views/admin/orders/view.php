<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>
<div class="orders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'order_date',
            'summa',
            'coord_x',
            'coord_y',
            'time_last:datetime',
            'distance',
            'time_max:datetime',
            'status',
        ],
    ]) ?>

</div>
