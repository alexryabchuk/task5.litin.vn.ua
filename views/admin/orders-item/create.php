<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrdersItem */

?>
<div class="orders-item-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
