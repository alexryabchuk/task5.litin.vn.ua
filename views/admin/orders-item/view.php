<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersItem */
?>
<div class="orders-item-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'orders_id',
            'product_id',
            'amount',
            'summa',
        ],
    ]) ?>

</div>
