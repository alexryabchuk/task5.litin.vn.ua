<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersItem */
?>
<div class="orders-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
