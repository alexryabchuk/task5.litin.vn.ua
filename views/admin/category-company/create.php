<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CategoryCompany */

?>
<div class="category-company-create">
    <?= $this->render('_form', [
        'model' => $model,
        
    ]) ?>
</div>
