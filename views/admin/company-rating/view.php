<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyRating */
?>
<div class="company-rating-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'rate',
            'respond',
        ],
    ]) ?>

</div>
