<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;


$path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';

 
?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == null | $session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?=$path?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?=$path?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?=Yii::$app->user->identity->id?></div>
                                <div class="profile-data-title"><?=Yii::$app->user->identity->id?></div>
                            </div>
                            <div class="profile-controls">
                                <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-left']); ?>

                                <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-right']); ?>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Menu</li>
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Кагетории компаний</span>', ['admin/category-company/index'], []); ?>
                    </li> 
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Категории продуктов</span>', ['admin/category-product/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Компании</span>', ['admin/company/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Отзиви</span>', ['admin/company-rating/index'], []); ?>
                    </li> 
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Товари</span>', ['admin/product/index'], []); ?>
                    </li> 
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Закази</span>', ['admin/orders/index'], []); ?>
                    </li> 
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Клиенти</span>', ['admin/clients/index'], []); ?>
                    </li> 
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Продукти</span>', ['admin/users/index'], []); ?>
                    </li> 
                                
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->