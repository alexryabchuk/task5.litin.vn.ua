<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m181115_231130_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'company_id'=> $this->integer()->notNull(),
            'name' => $this->string()->notNull()->unique()->comment('Название'),
            'description' => $this->string(32)->notNull()->comment('Описание'),
            'weight' => $this->string()->notNull()->comment('Вес'),
            'picture' => $this->string()->unique()->comment('Картинка'),
            'price' => $this->string(12)->notNull()->unique()->comment('Цена'),
            'time_prepare' => $this->smallInteger()->notNull()->defaultValue(10)->comment('Время на приготовление'),
            'valid' => $this->integer()->notNull()->comment('Проверенно'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Статус'),
            'category_product_id'=> $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex('idx-product-company_id', 'product', 'company_id');
        $this->addForeignKey('fk-product-company_id', 'product', 'company_id', 'company', 'id', 'CASCADE');
        $this->createIndex('idx-product-category_product_id', 'product', 'category_product_id');
        $this->addForeignKey('fk-product-category_product_id', 'product', 'category_product_id', 'category_product', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}
