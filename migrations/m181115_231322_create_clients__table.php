<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients_`.
 */
class m181115_231322_create_clients__table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients_', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('clients_');
    }
}
