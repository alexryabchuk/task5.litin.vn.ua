<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'time_last', 'time_max', 'status'], 'integer'],
            [['order_date'], 'safe'],
            [['summa', 'coord_x', 'coord_y', 'distance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'order_date' => $this->order_date,
            'summa' => $this->summa,
            'coord_x' => $this->coord_x,
            'coord_y' => $this->coord_y,
            'time_last' => $this->time_last,
            'distance' => $this->distance,
            'time_max' => $this->time_max,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
