<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $company_id
 * @property string $order_date
 * @property string $summa Сума
 * @property string $coord_x Координата X
 * @property string $coord_y Координата Y
 * @property int $time_last Время до прибытия
 * @property string $distance Расстояние до места
 * @property int $time_max Время на заказ
 * @property int $status
 *
 * @property Company $company
 * @property OrdersItem[] $ordersItems
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'time_last', 'time_max', 'status'], 'integer'],
            [['order_date'], 'safe'],
            [['summa', 'coord_x', 'coord_y', 'distance'], 'number'],
            [['coord_x', 'coord_y'], 'required'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'order_date' => 'Order Date',
            'summa' => 'Сума',
            'coord_x' => 'Координата X',
            'coord_y' => 'Координата Y',
            'time_last' => 'Время до прибытия',
            'distance' => 'Расстояние до места',
            'time_max' => 'Время на заказ',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersItems()
    {
        return $this->hasMany(OrdersItem::className(), ['orders_id' => 'id']);
    }
}
