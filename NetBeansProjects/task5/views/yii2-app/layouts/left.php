<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

if (!file_exists('avatars/'.Yii::$app->user->identity->foto) || Yii::$app->user->identity->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.Yii::$app->user->identity->foto;
}
 
?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == null | $session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?=$path?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?=$path?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?=Yii::$app->user->identity->fio?></div>
                                <div class="profile-data-title"><?=Yii::$app->user->identity->getTypeDescription()?></div>
                            </div>
                            <div class="profile-controls">
                                <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-left']); ?>
                                <!-- <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a> -->
                                <!-- <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a> -->
                                <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-right']); ?>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Menu</li>
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Пользователи</span>', ['/users/index'], []); ?>
                    </li> 
                    <li>
                        <?= Html::a('<span class="fa fa-files-o"></span> <span class="xn-text">Создать резюме</span>', ['/create-resume/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-file"></span> <span class="xn-text">Образование</span>', ['/education/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-file"></span> <span class="xn-text">Опыт работы</span>', ['/experience/index'], []); ?>
                    </li> 
                    <li>
                        <?= Html::a('<span class="fa fa-home"></span> <span class="xn-text">Город</span>', ['/city/index'], []); ?>
                    </li> 
                    <li>
                        <?= Html::a('<span class="fa fa-cogs"></span> <span class="xn-text">Настройки</span>', ['/settings/index'], []); ?>
                    </li> 
                                
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->