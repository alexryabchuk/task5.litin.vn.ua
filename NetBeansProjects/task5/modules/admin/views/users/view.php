<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'username',
            'password_hash',
            'password_reset_token',
            'phone',
            'created_at',
            'updated_at',
            'fullname',
            'post',
            'status',
        ],
    ]) ?>

</div>
