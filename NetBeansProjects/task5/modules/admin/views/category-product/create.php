<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CategoryProduct */

?>
<div class="category-product-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
