<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders_item_`.
 */
class m181115_231224_create_orders_item__table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders_item', [
            'id' => $this->primaryKey(),
            'orders_id'=>$this->integer(),
            'product_id'=>$this->integer()->comment('Продукт'),
            'amount'=>$this->integer()->comment('Количество'),
            'summa'=>$this->integer()->comment('Сумма'),
        ]);
        $this->createIndex('idx-orders_item-orders_id', 'orders_item', 'orders_id');
        $this->addForeignKey('fk-orders_item-orders_id', 'orders_item', 'orders_id', 'orders', 'id', 'CASCADE');
        $this->createIndex('idx-orders_item-product_id', 'orders_item', 'product_id');
        $this->addForeignKey('fk-orders_item-product_id', 'orders_item', 'product_id', 'product', 'id', 'CASCADE');
    }
    


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders_item_');
    }
}
